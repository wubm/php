FROM ubuntu:16.04
MAINTAINER wubm <bingmu@gmail.com>

# ENV VAR
ENV HOME=/root WORKDIR=/var/www/html/TemphawkWeb
ENV TERM xterm

# Switch to /root
WORKDIR $HOME

# Setting time zone
ADD ./docker/Timezone_Taipei /etc/localtime

# Update and install package
RUN apt-get update -y \
    && apt-get install -y software-properties-common \
    && LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php \
    && apt-get update -y \
    && apt-get install -y git curl unzip vim apache2 php5.6 mysql-client net-tools traceroute iputils-ping\
            php5.6-json libapache2-mod-php5.6 php5.6-zip php5.6-mcrypt php5.6-mbstring php5.6-xml php5.6-mysql php5.6-mongo

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Insatll laravel
RUN composer global require "laravel/installer" \
    && ln -s /root/.composer/vendor/laravel/installer/laravel /bin/laravel

# Setup apache rewrite mod
RUN a2enmod rewrite

# Setup temphawk web service
COPY . $WORKDIR
WORKDIR $WORKDIR
RUN composer install --no-scripts

# Setup apache rewrite mod
RUN a2enmod rewrite \
    && cp $WORKDIR/docker/TemphawkWeb.conf /etc/apache2/sites-available/TemphawkWeb.conf \
    && a2dissite 000-default.conf \
    && a2ensite TemphawkWeb.conf

# Export port
EXPOSE 80 443

RUN cp $WORKDIR/docker/docker-entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["-production"]
